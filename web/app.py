from flask import Flask, render_template, abort

app = Flask(__name__)

@app.route("/<path:name>")
def show(name):
	if '//' in name or '~' in name or '..' in name:
		return abort(403)
	else:
		try:	
			return render_template(name)
		except Exception as e:
			return abort(404)	

@app.errorhandler(404)
def error_404(error):
	return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(error):
	return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
