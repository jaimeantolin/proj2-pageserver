docker container stop $(docker container ls -aq)
docker container rm $(docker container ls -aq)
docker image rm server
docker build -t server .
docker run -d -p 5000:5000 server
